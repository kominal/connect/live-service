import service from '..';
import { SocketConnectionDatabase } from '@kominal/connect-models/socketconnection/socketconnection.database';
import StatusDatabase from '@kominal/connect-models/status/status.database';
import { Connection } from '../connection';
import { debug } from '@kominal/service-util/helper/log';

export class StatusHandler {
	static async onUserRefresh(connection: Connection, removedInterestingUserIds: string[], addedInterestingUserIds: string[]) {
		for (const userId of removedInterestingUserIds) {
			connection.socket.emit('EVENT', { type: 'STATUS', body: { userId, status: 'OFFLINE' } });
		}

		for (const userId of addedInterestingUserIds) {
			const statusSetting = await StatusDatabase.findOne({ userId });
			const status = statusSetting?.get('status') || 'ONLINE';
			if (status != 'OFFLINE') {
				connection.socket.emit('EVENT', { type: 'STATUS', body: { userId, status } });
			}
		}
	}

	static async onUserConnect(connection: Connection) {
		const { userId, interestingUserIds } = connection;

		const statusSetting = await StatusDatabase.findOne({ userId });
		const status = statusSetting?.get('status') || 'ONLINE';

		let sent = false;
		if (status != 'OFFLINE' && (await SocketConnectionDatabase.countDocuments({ userId })) === 1) {
			service.getSMQClient().publish('TOPIC', `PUBLIC.USER.${userId}`, 'STATUS', { userId, status });
			debug(`Sending ${status} for ${userId} to topic...`);
			sent = true;
		}

		const interestingConnections = await SocketConnectionDatabase.find({ userId: { $in: interestingUserIds } });
		const interestingUsers: string[] = [];
		for (const socketConnection of interestingConnections) {
			const socketUserId = String(socketConnection.get('userId'));
			if ((!sent || socketUserId != userId) && !interestingUsers.includes(socketUserId)) {
				interestingUsers.push(socketUserId);
			}
		}

		for (const socketUserId of interestingUsers) {
			const statusSetting = await StatusDatabase.findOne({ userId: socketUserId });
			const status = statusSetting?.get('status') || 'ONLINE';
			if (status != 'OFFLINE') {
				debug(`Sending ${status} for ${socketUserId} to socket...`);
				connection.socket.emit('EVENT', { type: 'STATUS', body: { userId: socketUserId, status } });
			}
		}
	}

	static async onUserDisconnect(userId: string) {
		const statusSetting = await StatusDatabase.findOne({ userId });
		const status = statusSetting?.get('status') || 'ONLINE';

		if (status != 'OFFLINE' && (await SocketConnectionDatabase.countDocuments({ userId })) === 0) {
			service.getSMQClient()?.publish('TOPIC', `PUBLIC.USER.${userId}`, 'STATUS', { userId, status });
		}
	}
}
