import { info } from '@kominal/service-util/helper/log';
import service from '.';
import { SocketConnectionDatabase } from '@kominal/connect-models/socketconnection/socketconnection.database';
import { StatusHandler } from './handler/handler.status';
import { CallHandler } from './handler/handler.call';
export async function cleanup() {
	info('Starting sync job...');
	try {
		const taskIds = await service.getSquad().getTaskIds();
		if (taskIds.length > 0) {
			const socketConnections = await SocketConnectionDatabase.find({ taskId: { $not: { $in: taskIds } } });
			const result = await SocketConnectionDatabase.deleteMany({ taskId: { $not: { $in: taskIds } } });
			for (const socketConnection of socketConnections) {
				const { connectionId, userId, groupId } = socketConnection.toJSON();
				await StatusHandler.onUserDisconnect(userId);
				await CallHandler.onUserDisconnect(connectionId, userId, groupId);
			}
			if (result.deletedCount && result.deletedCount > 0) {
				info(`Removed ${result.deletedCount} dead socket connections!`);
			}
		} else {
			info(`No tasks found!`);
		}
	} catch (e) {
		console.log(e);
	}
}
