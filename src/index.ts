import Service from '@kominal/service-util/helper/service';
import { SMQHandler } from './handler.smq';
import { cleanup } from './cleanup';
import { Connection } from './connection';
import status from './routes/status';
import check from './check';
import relay from './routes/relay';

const service = new Service({
	id: 'live-service',
	name: 'Live Service',
	description: 'Manages live connections.',
	jsonLimit: '16mb',
	routes: [status, relay],
	squad: true,
	database: 'live-service',
	scheduler: [
		{ task: cleanup, squad: true, frequency: 30 },
		{ task: check, squad: false, frequency: 30 },
	],
	socketHandler: async (socket) => {
		new Connection(socket);
	},
	swarmMQ: new SMQHandler(),
});
service.start();

export default service;
